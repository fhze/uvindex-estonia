.POSIX:

include config.mk

all:

install:
	# Install executable
	mkdir -p "$(DESTDIR)$(BINDIR)"
	cp uvindex "$(DESTDIR)$(BINDIR)/$(PROGNAME)"
	chmod 755 "$(DESTDIR)$(BINDIR)/$(PROGNAME)"

uninstall:
	rm -f "$(DESTDIR)$(BINDIR)/$(PROGNAME)"

.PHONY: all install uninstall
