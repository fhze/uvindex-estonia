# uvindex-estonia

Small Lua script to fetch UV Index data for Estonian weather stations.

## Dependencies

  * [Lua 5.2](https://www.lua.org/)
  * [LuaExpat](https://lunarmodules.github.io/luaexpat/)
  * [lua-curl](https://tracker.debian.org/pkg/lua-curl)

Note: *lua-curl* appears to be entirely maintained by the Debian Maintenance
Team, as the upstream seems to have been abandoned. I might use a different
library in the future.

### Debian

	apt-get install lua5.2 lua-expat lua-curl

## Installation

Edit `config.mk`, and then run:

	make install

## Usage

	uvindex

---

Licenced under the MIT License, see `LICENSE`.
